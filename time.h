#include "stm32f0xx.h"

volatile static int count = 0;
volatile static int timestamp = 0;

void TimeInit (void);
void wait (int x);
void StartPeriod (void);
int EndPeriod(void);

#include "time.h"

void TimeInit ()
{
	SystemCoreClockUpdate();
	
	SysTick->CTRL	= SysTick->CTRL | SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk;
	SysTick->LOAD = SystemCoreClock/1000 - 1;
}


void SysTick_Handler ()
{
	count++;
}

void wait (int x)
{
	count = 0;
	while (count < x);
}

void StartPeriod ()
{
	timestamp = count;
}

int EndPeriod()
{
	return (count - timestamp);
}
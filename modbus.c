#include "modbus.h"

void ParseMessage()
{
	device_addr = GetRecievedData(0);
	command_code = GetRecievedData(1);
	reg_addr1 = GetRecievedData(2);
	reg_addr0 = GetRecievedData(3);
	number1 = GetRecievedData(4);
	number0 = GetRecievedData(5);
}



void ReadCoilStats ()
{
	if (Check_CRC() == 1)	

	{

	}
}

void ReadDiscreteInput ()
{
	if (Check_CRC() == 1)
	
	{

	}
}

void ReadHoldingRegisters ()
{
	if (Check_CRC() == 1)	

	{

	}
}

void ReadInputRegisters ()
{
	
	
	
	if (Check_CRC() == 1)
	
	{
		
//		GetAngles();
//		
//		SetTrIter(0);
//		SetTrFlag();
//		
//		USART3->CR1 |= USART_CR1_TXEIE;
		
			push_char(device_addr);
			push_char(command_code);
			push_char(0x04);

			GetAngles();
		
			uint16_t control = GetCRC (GetTransmitDataPointer(), GetTransmitDataLen());
	
			uint8_t low = control >> 8;
			uint8_t high = control;
		
			push_char(low);
			push_char(high);
			
			//wait(5000);
			
			TransmitStart();
	}
}

int Check_CRC ()
{
	uint16_t control = GetCRC (GetRecieveDataPointer(), GetRecieveDataLen()-2);
	
	uint8_t low = control >> 8;
	uint8_t high = control;
	
	clear_transmit_data();
	
	if ((low==GetRecievedData(GetReIter()-2))&&(high==GetRecievedData(GetReIter()-1)))
	{
		return 1;
	}
	else
	{
		return 0;	
	}
}

void TypeOfCommand()
{
	ParseMessage();

	switch (command_code)
	{
		case _R_COIL_STATUS: ReadCoilStats(); break;
		case _R_DISCRETE_INPUT: ReadDiscreteInput(); break;
		case _R_HOLDING_REGISTERS: ReadHoldingRegisters(); break;
		case _R_INPUT_REGISTERS: ReadInputRegisters(); break;
		default: break;
	}
}